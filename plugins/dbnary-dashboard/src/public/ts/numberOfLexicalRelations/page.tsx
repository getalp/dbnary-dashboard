import React, { FC } from "react";
import { MainBarChart } from "./mainBarChart";
import { decorations } from "./styles";

/* istanbul ignore next: Example implementations gets deleted the most time after plugin creation! */
const NumberOfLexicalRelations: FC<{}> = () => {
    return <MainBarChart decorations={decorations} />;
};

export { NumberOfLexicalRelations };
