import React, { FC } from "react";
import { MainBarChart } from "./mainBarChart";
import { decorations } from "./styles";

/* istanbul ignore next: Example implementations gets deleted the most time after plugin creation! */
type param = {
    langue: string;
};
const TranslationGlossesByLanguage = ({ langue }) => {
    return <MainBarChart decorations={decorations} langue={langue} />;
};

export { TranslationGlossesByLanguage };
