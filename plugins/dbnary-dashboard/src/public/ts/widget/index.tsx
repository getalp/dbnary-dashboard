import React, { FC } from "react";
import { decorations } from "../dashboard/styles";
import { PagesCard } from "../dashboard/statsLine";
import { __ } from "../utils";

/* istanbul ignore next: Example implementations gets deleted the most time after plugin creation! */
const Widget: FC<{}> = () => (
    <div className="react-boilerplate-widget">
        <h3>{__("Hello, World!")}</h3>
        <p>{__("I got generated from your new plugin!")}</p>
    </div>
);

const DbnaryWidget: FC<{}> = () => <PagesCard decorations={decorations} langue="General" />;

export { Widget, DbnaryWidget };
