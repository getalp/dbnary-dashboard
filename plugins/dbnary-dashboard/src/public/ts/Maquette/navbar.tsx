import { AppBar, makeStyles, Tab, Tabs, Box } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { MainBarChart } from "./mainBarChart";

import { doAllLanguagesForNavBar, SparqlResponse, TypedValue } from "../wp-api";
import { StatsLine } from "../dashboard/statsLine";
import { decorations } from "./styles";
import { MaquetteByLanguages } from "./MaquetteByLanguages";
import { getEnglishName } from "../utils/iso636_1";
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </div>
    );
}
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.any.isRequired
};
function valueAsString(val: TypedValue): string {
    return val.value;
}
const types: Record<string, (tval: TypedValue) => any> = {
    Language: valueAsString
};
function normalizeSparqlData(response: SparqlResponse): Array<Record<string, any>> {
    const result: Array<Record<string, any>> = [];
    // console.log("Normalizing data");
    // console.log(response.results.bindings);
    if (response && response.results && response.results.bindings && response.results.bindings.length > 0) {
        response.results.bindings.forEach((record: Record<string, TypedValue>) => {
            const resultRec: Record<string, any> = {};
            Object.keys(record).forEach((key: string) => {
                if (types[key]) resultRec[key] = types[key](record[key]);
                else resultRec[key] = record[key].value;
            });
            result.push(resultRec);
        });
    }
    return result;
}

const langNameFormatter = (label: any) => {
    return label instanceof Number ? <span>{label}</span> : <span>{getEnglishName(label)}</span>;
};

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`
    };
}

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1
    }
}));
export function Navbar() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [data, setData] = useState<Array<Record<string, any>>>([{ Language: "bg" }]);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    useEffect(() => {
        doAllLanguagesForNavBar().then(normalizeSparqlData).then(setData);
    }, []);
    return (
        <div>
            <AppBar className={classes.root} position="static">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    aria-label="simple tabs example"
                    variant="scrollable"
                    scrollButtons="auto"
                >
                    <Tab label="General" {...a11yProps(0)} />
                    {data.map((label, i) => {
                        return <Tab key={i} label={langNameFormatter(label.Language)} {...a11yProps(i)} />;
                    })}
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                <StatsLine decorations={decorations} langue="General" />
                <MainBarChart />
            </TabPanel>
            {data.map((label, i) => {
                return (
                    <TabPanel value={value} index={i + 1} key={i + 1}>
                        <StatsLine decorations={decorations} langue={label.Language} />
                        <MaquetteByLanguages langue={label.Language} />
                    </TabPanel>
                );
            })}
        </div>
    );
}
