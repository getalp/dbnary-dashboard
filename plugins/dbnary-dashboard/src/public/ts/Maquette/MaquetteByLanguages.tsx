import React, { Component, FC, useEffect, useState } from "react";
import { NumberOfElementsByLanguage } from "../numberOfElementsByLanguage/numberOfElementsForFr";
import { NumberOfLexicalRelationsByLanguage } from "../numberOfLexicalRelationsByLanguage/numberOfLexicalRelationsForFr/";
import { NumberOfTranslationsByLanguage } from "../numberOfTranslationsByLanguages/numberOfTranslationsForFr/";
import { EnhancementConfidenceByLanguage } from "../enhancementConfidenceByLanguage";
import { TranslationGlossesByLanguage } from "../translationGlossesByLanguage";
import Grid from "@material-ui/core/Grid";

import { DecorationSpec } from "./styles";

/* The decorations to provide to the generic barchart */

const MaquetteByLanguages = ({ langue }) => {
    return (
        <Grid container item xs={12} spacing={3} justify="center" alignItems="center">
            <Grid item xs={12} sm={4}>
                <NumberOfElementsByLanguage langue={langue} />
            </Grid>
            <Grid item xs={12} sm={4}>
                <NumberOfLexicalRelationsByLanguage langue={langue} />
            </Grid>
            <Grid item xs={12} sm={4}>
                <NumberOfTranslationsByLanguage langue={langue} />
            </Grid>
            <Grid item xs={12} sm={6}>
                <EnhancementConfidenceByLanguage langue={langue} />
            </Grid>
            <Grid item xs={12} sm={6}>
                <TranslationGlossesByLanguage langue={langue} />
            </Grid>
        </Grid>
    );
};

export { MaquetteByLanguages };
