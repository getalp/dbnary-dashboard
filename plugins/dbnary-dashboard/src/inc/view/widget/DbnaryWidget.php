<?php
namespace GillesSerasset\DbnaryDashboard\view\widget;
use GillesSerasset\DbnaryDashboard\base\UtilsProvider;
use WP_Widget;

// @codeCoverageIgnoreStart
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request
// @codeCoverageIgnoreEnd

/**
 * DBnary widget that creates a small card with general statistics.
 *
 * @codeCoverageIgnore Example implementations gets deleted the most time after plugin creation!
 */
class DbnaryWidget extends WP_Widget {
    use UtilsProvider;

    /**
     * C'tor.
     */
    public function __construct() {
        $widget_ops = [
            // Just for fun _n() usage demonstrating prulars i18n
            'description' => _n(
                'A widget that shows DBnary general stats.',
                'Widgets that show DBnary general stats.',
                1,
                DBNARY_DASHBOARD_TD
            )
        ];
        parent::__construct(DBNARY_DASHBOARD_TD . 'dbnary-stats-widget', 'DBnary Stats Widget', $widget_ops);
    }

    /**
     * Output the widget content.
     *
     * @param mixed $args
     * @param array $instance
     */
    public function widget($args, $instance) {
        echo $args['before_widget']; ?>
<div class="dbnary-stats-widget-wrapper"></div>
        <?php echo $args['after_widget'];
    }

    /**
     * Updates a particular instance of a widget.
     *
     * @param array $new_instance
     * @param array $old_instance
     */
    public function update($new_instance, $old_instance) {
        // Silence is golden.
    }

    /**
     * Outputs the settings update form.
     *
     * @param array $instance
     * @return string
     */
    public function form($instance) {
        return 'noform';
    }
}
